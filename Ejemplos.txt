
TODAS LAS SIGUIENTES ENTRADAS SON VALIDAS PARA EL PROGRAMA


        LEYES DE MORGAN:

            1   (no (A y B)) es lo mismo que (no A o no B)
            1   (p^q) -> (~pv~q)
            2   (no (A o B)) es lo mismo que (no A y no B)
            2   (pvq) -> (~p^~q)
            3   negar ( A v B) <-> ( negar a y negar b)
            3   ~(A+B)<->(~A^~B)


        DEDUCCION NATURAL:

            Introduccion a la negacion / Regla de inferencia
            1   ( p entonces q) y (p entonces negacion de q) se infiere negar p
            1   (P->Q)^(P->~Q)->~P

            Eliminacion de la negacion
            2   negar p se infiere ( p entonces r)
            2   ~P->(P->R)

            Elimacion de la doble negacion
            3   negar negar p se infiere p
            3   ~~P->P

            Introduccion de la negacion
            4   p,q se infiere ( p ^ q)
            4   P^Q->(P^Q)

            Eliminacion de la conjuncion
            5   ( p ^ q ) se infiere p
            5   (P^Q)->P
            6   ( p ^ q ) se infiere q
            6   (P^Q)->Q
            7   ((p y q) entonces p) y (( p y q) entonces q)
            7   ((P^Q)->P)^((P^Q)->Q)

            Introduccion a la disyuncion
            8   De p se infiere (p v q)
            8   P->(P+Q)
            9   De p se infiere ( p v q) y q se infiere ( p v q)
            9   P->(P+Q)^Q->(P+Q)

            Eliminacion de la disyuncion
            10  De (p v q) y ( p entonces r) y (q entonces r) se infiere r
            10  (P+Q)^(P->R)^(Q->R)->R

            Introduccion del Bicondicional
            11  (p y q) y (q y p) = (p<->p)
            11  (P^Q)^(Q^P)->(P<->P)

            Eliminacion del Bicondicional
            12  {(pvq),(p->r),(q->r)] = r
            12  ((P+Q)^(P->R)^(Q->R))->R

            Modus Ponens (Eliminacion del Condicional)
            13  p y (p -> q) se infiere q
            13 {p,(p->q)} = q
            13  P^(P->Q)->Q

            Prueba Condicional (Introduccion del Condicional)
            14  (p = q) = ( p ->q)
            14  (P->Q)->(P->Q)

        FORMAS DE ARGUMENTOS BASICAS Y DERIVADAS:

            Modus Ponens (Eliminacion del Condicional)
            15  p y (p -> q) se infiere q
            15  P^(P->Q)->Q

            Modus Tollens
            16  ((p ->q) ^p) = q

            Silogismo Hipotetico
            17  ((p -> q) ^ (q -> r)) = (p ->r)

            Silogismo Disyuntivo
            18  (( p o q) y negar p) por lo tanto q
            18  ((P+Q)^~P)->Q

            Dilema Constructivo
            19  ((p entonces q)  y (r entonces s) y (p or r)) entonces (q or s)
            19  ((P->Q)^(R->S)^(P+R))->(Q+S)

            Dilema Bidireccional
            20  ((p entonces q) y ( r entonces s) y (p v negar s)) es igual (q v negar r)
            20  ((P->Q)^(R->S)^(P+~S))->(Q+~R)

            Composicion
            21  (( p entonces q) y (p entonces r)) por lo tanto ( p entonces (q y r))
            21  ((P->Q)^(P->R))->(P->(Q^R))


        FORMAS NORMALES
            (P->Q)<->(~P+Q)
            (P<->Q)<->((~P+Q)^(~Q+P))

        EQUIVALENCIAS DE UN CONJUNTO COMPLETO

            Puertas NAND:
                1   a NAND a = negar a
                1   A NAND A->~A
                2   (a nand b) nand (a nand b) = a y b
                2   (A NAND B) NAND (A NAND B)->A^B
                3   (a nand a) nand (b nand b) = a o b
                3   (A NAND A) NAND (B NAND B)->A+B
                4   ((b NAND b) NAND (a NAND a)) NAND ( b nand b) = (a -> b)
                4   (B NAND B) NAND (A NAND A) NAND (B NAND B)->(A->B)

            Puertas NOR:
                1   a nor a = not a
                1   A NOR A->~A
                2   (a nor b) nor ( a nor b) = a o b
                2   (A NOR B) NOR (A NOR B)->A+B
                3   (a nor a) nor ( b nor b) = a y b
                3   (A NOR B) NOR (A NOR B)->A+B
                4   ((a nor a) nor b) nor ( (b nor b) nor a) = (a ->b)
                4   ((A NOR A) NOR B) NOR ((B NOR B) NOR A)->(A->B)

            Pseudo Asociatividad y Pseudo Distributividad de NOR y NAND
                1   A nor negar ( b nor c) equivale a negar ( a nor b) nor c
                1   A NOR ~(B NOR C)<->~(A NOR B) NOR C
                2   a nand negar (b nand c) equivale a negar( a nand b) nand c
                2   A NAND ~(B NAND C)<->~(A NAND B) NAND C
                3   a nor negar(b nand c) equivale (negar (a nor b) nand negacion de (a nor c))
                3   A NOR ~(B NAND C)<->(~(A NOR B) NAND ~(A NOR C))
                4   a nand negar ( b nor c) equivale a  negar (a nand b) nor negar(a nand c)
                4   A NAND ~(B NOR C)<->~(A NAND B) NOR ~(A NAND C)









-------------------------------------------------------------------------------------
	Ingresa la proposicion logica:
	((p entonces q) y (q entonces r)) entonces (p entonces r)

	Proposicion formalizada:  ((P->Q)^(Q->R))->(P->R)

	Numero de combinaciones:  8

	Considere:

	1 -> (P->R)
	2 -> (Q->R)
	3 -> (P->Q)
	4 -> ((P->Q)^(Q->R))
	5 -> ((P->Q)^(Q->R))->(P->R)

		TABLA DE VERDAD

	P Q R 1 2 3 4 5 

	0 0 0 1 1 1 1 1 

	0 0 1 1 1 1 1 1 

	0 1 0 1 0 1 0 1 

	0 1 1 1 1 1 1 1 

	1 0 0 0 1 0 0 1 

	1 0 1 1 1 0 0 1 

	1 1 0 0 0 1 0 1 

	1 1 1 1 1 1 1 1

-------------------------------------------------------------------------------------

	Ingresa la proposicion logica:
	negar[(pvq)y(negar q entonces negar p)]

	Proposicion formalizada:  ~((P+Q)^(~Q->~P))

	Numero de combinaciones:  4

	Considere:

	1 -> ~Q
	2 -> ~P
	3 -> (~Q->~P)
	4 -> (P+Q)
	5 -> ((P+Q)^(~Q->~P))
	6 -> ~((P+Q)^(~Q->~P))

		TABLA DE VERDAD

	P Q 1 2 3 4 5 6 

	0 0 1 1 1 0 0 1 

	0 1 0 1 1 1 1 0 

	1 0 1 0 0 1 0 1 

	1 1 0 0 1 1 1 0 

-------------------------------------------------------------------------------------
	Ingresa la proposicion logica:
	((p -> q) and !q)entonces not p

	Proposicion formalizada:  ((P->Q)^~Q)->~P

	Numero de combinaciones:  4

	Considere:

	1 -> (P->Q)
	2 -> ~Q
	3 -> ((P->Q)^~Q)
	4 -> ~P
	5 -> ((P->Q)^~Q)->~P

		TABLA DE VERDAD

	P Q 1 2 3 4 5 

	0 0 1 1 1 1 1 

	0 1 1 0 0 1 1 

	1 0 0 1 0 0 1 

	1 1 1 0 0 0 1 

	


		Proposicion tautologica / Tautologia


-------------------------------------------------------------------------------------
	Ingresa la proposicion logica:
	(not (p v q) and negar p) si solo si (q entonces p)

	Proposicion formalizada:  (~(P+Q)^~P)<->(Q->P)

	Numero de combinaciones:  4

	Considere:

	1 -> (Q->P)
	2 -> (P+Q)
	3 -> ~(P+Q)
	4 -> ~P
	5 -> (~(P+Q)^~P)
	6 -> (~(P+Q)^~P)<->(Q->P)

		TABLA DE VERDAD

	P Q 1 2 3 4 5 6 

	0 0 1 0 1 1 1 1 

	0 1 0 1 0 1 0 1 

	1 0 1 1 0 0 0 0 

	1 1 1 1 0 0 0 0 

	Verdad indeterminada / Contingencia


-------------------------------------------------------------------------------------

		

	Ingresa la proposicion logica:
	negar ((p y q) entonces negar (negar q v negar p))

	Proposicion formalizada:  ~((P^Q)->~(~Q+~P))

	Numero de combinaciones:  4

	Considere:

	1 -> ~Q
	2 -> ~P
	3 -> (~Q+~P)
	4 -> (P^Q)
	5 -> ~(~Q+~P)
	6 -> ((P^Q)->~(~Q+~P))
	7 -> ~((P^Q)->~(~Q+~P))

		TABLA DE VERDAD

	P Q 1 2 3 4 5 6 7 

	0 0 1 1 1 0 0 1 0 

	0 1 0 1 1 0 0 1 0 

	1 0 1 0 1 0 0 1 0 

	1 1 0 0 0 1 1 1 0 

	Proposicion contradictoria / Contradiccion

-------------------------------------------------------------------------------------	


	Ingresa la proposicion logica:
	((p entonces q) y negar q) entonces negar p

	Proposicion formalizada:  ((P->Q)^~Q)->~P

	Numero de combinaciones:  4

	Considere:

	1 -> (P->Q)
	2 -> ~Q
	3 -> ((P->Q)^~Q)
	4 -> ~P
	5 -> ((P->Q)^~Q)->~P

		TABLA DE VERDAD

	P Q 1 2 3 4 5 

	0 0 1 1 1 1 1 

	0 1 1 0 0 1 1 

	1 0 0 1 0 0 1 

	1 1 1 0 0 0 1 

	


		Proposicion tautologica / Tautologia


-------------------------------------------------------------------------------------

	Ingresa la proposicion logica:
	((p v q) y q) entonces negar p

	Proposicion formalizada:  ((P+Q)^Q)->~P

	Numero de combinaciones:  4

	Considere:

	1 -> (P+Q)
	2 -> ((P+Q)^Q)
	3 -> ~P
	4 -> ((P+Q)^Q)->~P

		TABLA DE VERDAD

	P Q 1 2 3 4 

	0 0 0 0 1 1 

	0 1 1 1 1 1 

	1 0 1 0 0 1 

	1 1 1 1 0 0 

	


		Verdad indeterminada / Contingencia

-------------------------------------------------------------------------------------

	Ingresa la proposicion logica:
	(((negar p y negar q) entonces negar r) y (negar p y negar q))entonces negar r

	Proposicion formalizada:  (((~P^~Q)->~R)^(~P^~Q))->~R

	Numero de combinaciones:  8

	Considere:

	1 -> ~P
	2 -> ~Q
	3 -> (~P^~Q)
	4 -> ~R
	5 -> ((~P^~Q)->~R)
	6 -> (((~P^~Q)->~R)^(~P^~Q))
	7 -> ~R
	8 -> (((~P^~Q)->~R)^(~P^~Q))->~R

		TABLA DE VERDAD

	P Q R 1 2 3 4 5 6 7 8 

	0 0 0 1 1 1 1 1 1 1 1 

	0 0 1 1 1 1 0 0 0 0 1 

	0 1 0 1 0 0 1 1 0 1 1 

	0 1 1 1 0 0 0 1 0 0 1 

	1 0 0 0 1 0 1 1 0 1 1 

	1 0 1 0 1 0 0 1 0 0 1 

	1 1 0 0 0 0 1 1 0 1 1 

	1 1 1 0 0 0 0 1 0 0 1 


	Proposicion tautologica / Tautologia

-------------------------------------------------------------------------------------

	Ingresa la proposicion logica:
	((p entonces (q y r)) y (p y q)) entonces r

	Proposicion formalizada:  ((P->(Q^R))^(P^Q))->R

	Numero de combinaciones:  8

	Considere:

	1 -> (P^Q)
	2 -> (Q^R)
	3 -> (P->(Q^R))
	4 -> ((P->(Q^R))^(P^Q))
	5 -> ((P->(Q^R))^(P^Q))->R

		TABLA DE VERDAD

	P Q R 1 2 3 4 5 

	0 0 0 0 0 1 0 1 

	0 0 1 0 0 1 0 1 

	0 1 0 0 0 1 0 1 

	0 1 1 0 1 1 0 1 

	1 0 0 0 0 0 0 1 

	1 0 1 0 0 0 0 1 

	1 1 0 1 0 0 0 1 

	1 1 1 1 1 1 1 1 

	


		Proposicion tautologica / Tautologia

-------------------------------------------------------------------------------------

	Ingresa la proposicion logica:
	((p entonces negar q) y negar p)entonces q

	Proposicion formalizada:  ((P->~Q)^~P)->Q

	Numero de combinaciones:  4

	Considere:

	1 -> ~Q
	2 -> (P->~Q)
	3 -> ~P
	4 -> ((P->~Q)^~P)
	5 -> ((P->~Q)^~P)->Q

		TABLA DE VERDAD

	P Q 1 2 3 4 5 

	0 0 1 1 1 1 0 

	0 1 0 1 1 1 1 

	1 0 1 1 0 0 1 

	1 1 0 0 0 0 1 

	


		Verdad indeterminada / Contingencia



